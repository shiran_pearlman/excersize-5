@extends('layouts.app')
@section('content')
@section('title','Candidates')

<body class='text-center'>
<div class="btn btn-outline-info"><a href="{{url('/candidates/create')}}">Add new candidates</a></div>
<h1>List of candidates</h1>
<table class="table">
  <thead class="thead-dark">
    <!-- insert the header of the table-->
    <tr>
        <th>id</th><th>name</th><th>email</th><th>owner</th><th>status</th><th>created</th><th>updated</th>
    </tr>
    <!-- insert the table data -->
    @foreach($candidates as $candidate)
        <tr>
        <td>{{$candidate->id}}</td>
        <td>{{$candidate->name}}</td>
        <td>{{$candidate->email}}</td>
        <td>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($candidate->user_id))
                        {{$candidate->owner->name}}
                    @else
                        Assign owner
                    @endif
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($users as $user)
                    <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id, $user->id])}}">{{$user->name}}</a>
                @endforeach
                </div>
            </div>
        </td>
        <td>
            <div class="dropdown">
                @if (null != App\Status::next($candidate->status_id))  
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($candidate->status_id))
                        {{$candidate->status->name}}
                    @else
                        Define status
                    @endif
                </button>
                @else
                {{$candidate->status->name}}
                @endif

                @if (App\Status::next($candidate->status_id)!=null)
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach(App\Status::next($candidate->status_id) as $status)
                    <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                @endforeach
                </div>
                @endif
            </div>
        </td>
        <td>{{$candidate->created_at}}</td>
        <td>{{$candidate->updated_at}}</td>
        <td><a href ="{{route('candidates.edit',$candidate->id)}}">Edit</td>
        <td><a href ="{{route('candidates.delete',$candidate->id)}}">Delete</td>                            
        </tr>
    @endforeach
</table>
</body> 
@endsection

