<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidates.delete');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth'); //מגדיר שרק משתמשים רשומים יכולים להגיע לROUTE הזה

Route::get('register', 'Auth\RegisterController@showForm');

Route::post('register', 'Auth\RegisterController@postForm')->name('register.department');


Route::get('/hello',function(){
    return 'Hello Laravel';
});

Route::get('/student/{id}',function($id='No student found'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}',function($id=null){ //כדי שתהיה אופציה שהמשתמש לא יכניס ערך למשתנה נוסיף סימן שאלה 
    if(isset($id)){ //פונקציה שבודקת האם קיים ערך משתנה כלשהו 
        //אפשר לעשות ולידציה של האם המשתנה הוא משתנה מהסוג שאנחנו מעוניינים בו
        return "We got car $id";
    }else{
        return 'We need the id to find your car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

Route::get('/users/{email}/{name?}',function($email=null,$name=null){  
    return view('users', compact('name','email'));
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
