<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidates()
    {
        return $this->hasMany('App\Candidate');
    } 

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        return self::find($nextstages)->all(); 
    }

    public static function allowed($from,$to){
        $allow = DB::table('nextstages')->where('from', $from)->where('to', $to)->get();
        if(isset($allow)) return true;
        return false;
      }

}
    
